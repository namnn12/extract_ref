WDIR=`pwd`
REF_PATH="/mnt/nas_share/namnn12/impute_data/ref_VN1008_1KGP/"
VCF_PATH="$WDIR/input/GSAv3_B4.rename.vcf.gz"
EXTRACTED_REF="/mnt/nas_share/namnn12/impute_data/ref_VN1008_1KGP_extract/"
mkdir -p $EXTRACTED_REF

extract () {
    CHR=$1

    WDIR=`pwd`
    REF_PATH="/mnt/nas_share/namnn12/impute_data/ref_VN1008_1KGP/"
    VCF_PATH="$WDIR/input/GSAv3_B4.rename.vcf.gz"
    EXTRACTED_REF="/mnt/nas_share/namnn12/impute_data/ref_VN1008_1KGP_extract/"

    # annot file
    VCF_FILE=`basename $VCF_PATH`
    TEMP_INPUT="$WDIR/temp_input/"
    mkdir -p $TEMP_INPUT
    bcftools norm -m-any -r $CHR $VCF_PATH -Oz -o "${TEMP_INPUT}${VCF_FILE/.vcf.gz/.${CHR}.vcf.gz}"
    bcftools annotate --set-id '%CHROM\_%POS\_%REF\_%FIRST_ALT' "${TEMP_INPUT}${VCF_FILE/.vcf.gz/.${CHR}.vcf.gz}" \
        -Oz -o "${TEMP_INPUT}${VCF_FILE/.vcf.gz/.${CHR}.annot.vcf.gz}"
    bcftools index -t "${TEMP_INPUT}${VCF_FILE/.vcf.gz/.${CHR}.annot.vcf.gz}"

    bcftools annotate --set-id '%CHROM\_%POS\_%REF\_%FIRST_ALT' "${REF_PATH}VN1008_1KGP.all.${CHR}.fix.Hap.norm.vcf.gz" \
        -Oz -o "${TEMP_INPUT}VN1008_1KGP.all.${CHR}.annot.fix.Hap.norm.vcf.gz"
    bcftools index -t "${TEMP_INPUT}VN1008_1KGP.all.${CHR}.annot.fix.Hap.norm.vcf.gz"

    # isec file
    bcftools query -f '%ID\n' "${TEMP_INPUT}${VCF_FILE/.vcf.gz/.${CHR}.annot.vcf.gz}" > "${TEMP_INPUT}id_${CHR}_genotyping.txt"
    bcftools view -i ID==@"${TEMP_INPUT}id_${CHR}_genotyping.txt" "${TEMP_INPUT}VN1008_1KGP.all.${CHR}.annot.fix.Hap.norm.vcf.gz" \
        -Oz -o "${EXTRACTED_REF}VN1008_1KGP.all.${CHR}.fix.Hap.norm.extract.vcf.gz"
    bcftools index -t "${EXTRACTED_REF}VN1008_1KGP.all.${CHR}.fix.Hap.norm.extract.vcf.gz"

}
export -f extract

parallel -j 12 extract chr{} ::: {1..22}